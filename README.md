# Simple Rewards
iOS app showing members the details of their earned rewards

## Notes

This build requires CocoaPods for Firebase. 

## Welcome Screen

|  |  |  |
|--|--|--|
| Login | button  | links to Login Screen 
| Register | button  | links to Register Screen 

## Login Screen

|  |  |  |
|--|--|--|
| Back | button  | links to Welcome Screen 
| Login | button  | links to Dashboard 
| Mobile Number | text  | format validation (09xxxxxxxxx)
| MPIN | text  | validation (4 digits)
| MPIN Toggle ON/OFF | button  | toggle MPIN secure text entry on/off

## Register Screen

|  |  |  |
|--|--|--|
| Register | button  | links to Dashboard 
| First Name | text  | Enter first name
| Last Name | text  | Enter last name 
| Mobile Number | text  | Enter Mobile Number / format validation (09xxxxxxxxx)
| MPIN | text  | Enter MPIN
| MPIN Confirm | text  | Confirm MPIN

## Dashboard

Header Info

|  |  |  |
|--|--|--|
| Users' Name | Label  | First Name + Last Name 
| Mobile Number | Label  | format (09xxxxxxxxx)
| Referral Code | Label  | Referral Code

Collection Cell View

|  |  |  |
|--|--|--|
| Cell| Cell  | links to Rewards Detail
| Rewards Image | Image  | 
| Rewards Caption | Label  | 

## Rewards Detail

|  |  |  |
|--|--|--|
| Rewards Image | Image  | 
| Rewards Name | Label  | 
| Rewards Description | Label  | 
| Share | button  | shares the rewards

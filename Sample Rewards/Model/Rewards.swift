//
//  Rewards.swift
//  Sample Rewards
//
//  Created by Chris Blay on 11/20/20.
//

import Foundation
struct Rewards {
    let rewards_id: String
    let name: String
    let description: String
    let image: String
    
    init(rewards_id: String, dictionary: [String:AnyObject]) {
        self.rewards_id = rewards_id
        self.name = dictionary["name"] as? String ?? ""
        self.description = dictionary["description"] as? String ?? ""
        self.image = dictionary["image"] as? String ?? ""
    }
}

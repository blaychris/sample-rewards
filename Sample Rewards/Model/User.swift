//
//  User.swift
//  Sample Rewards
//
//  Created by Chris Blay on 11/19/20.
//

import Foundation

struct User {
    let user_id: String
    let first_name: String
    let last_name: String
    let mobile: String
    let mpin: String
    let is_verified: Bool
    let referral_code: String
    
    init(user_id: String, dictionary: [String:AnyObject]) {
        self.user_id = user_id
        self.first_name = dictionary["first_name"] as? String ?? ""
        self.last_name = dictionary["last_name"] as? String ?? ""
        self.mobile = dictionary["mobile"] as? String ?? ""
        self.mpin = dictionary["mpin"] as? String ?? ""
        self.is_verified = dictionary["is_verified"] as? Bool ?? false
        self.referral_code = dictionary["referral_code"] as? String ?? ""
    }
}

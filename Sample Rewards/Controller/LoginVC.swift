//
//  LoginVC.swift
//  Sample Rewards
//
//  Created by Chris Blay on 11/19/20.
//

import UIKit

class LoginVC: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var txtMobileNo: UITextField!
    @IBOutlet weak var txtMPIN: UITextField!
    @IBOutlet weak var txtMPINBorder: UITextField!
    @IBOutlet weak var btnMPINToggle: UIButton!
    
    @IBOutlet weak var lblNotice: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    var usr : User!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap = UITapGestureRecognizer(target: view, action: #selector(UIView.endEditing))
        view.addGestureRecognizer(tap)
        
        let toolbar = UIToolbar()
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace,
                                        target: nil, action: nil)
        let btnKeyboardDone = UIBarButtonItem(title: "Done", style: .done,
                                         target: self, action: #selector(doneButtonTapped))
        
        toolbar.setItems([flexibleSpace, btnKeyboardDone], animated: true)
        toolbar.sizeToFit()
        
        txtMobileNo.inputAccessoryView = toolbar
        txtMPIN.inputAccessoryView = toolbar
     
    }
    
    @IBAction func backButtonTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func doneButtonTapped() {
        view.endEditing(true)
    }
    
    @IBAction func btnMPINToggleTapped(_ sender: UIButton) {
        
        if sender.tag == 0 {
            sender.setImage(UIImage.init(named: "password_show"), for: .normal)
        }else{
            sender.setImage(UIImage.init(named: "password_hide"), for: .normal)
        }
        sender.tag =  abs(sender.tag - 1)
        txtMPIN.isSecureTextEntry = (sender.tag == 0)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        lblNotice.text = ""
        txtMobileNo.layer.borderColor = UIColor.lightGray.cgColor        
        txtMPINBorder.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        if range.location < 2 && textField.tag == 0{
            if string == ""{
                return true
            }
            if !(((range.location == 0) && (string == "0")) || ((range.location == 1) && (string == "9"))){
                return false
            }
        }
        
        let maxLength = (textField.tag == 0) ? 11 : 4
        
        if range.location > maxLength - 1 {
            if textField.tag == 0 {
                txtMPIN.becomeFirstResponder()
            }else{
                doneButtonTapped()
            }
            return false
        }

        return true
    }
    
    @IBAction func btnLoginTapped() {
        validateInputs()
    }
    
    func validateInputs(){
        var strError = ""
        
        if txtMobileNo.text!.count != 11 {
            strError = "• Mobile Number should be in 09XXXXXXXXX format"
            txtMobileNo.layer.borderColor = UIColor.red.cgColor        }
        if txtMPIN.text!.count != 4 {
            if strError.count > 0 {
                strError = strError + "\r\n"
            }
            strError = strError + "• MPIN should be 4-digits"
            txtMPINBorder.layer.borderColor = UIColor.red.cgColor
        }
        lblNotice.text = strError
        activityIndicator.startAnimating()
        if strError.count == 0 {
            //handle login
            
            UserService.shared.fetchUserBy(mobileNo: txtMobileNo.text!) { (user) in
                self.usr = user
                if user.mpin != self.txtMPIN.text! {
                    let alert = UIAlertController(title: "", message: "Wrong Mobile number and/or MPIN", preferredStyle: UIAlertController.Style.alert)
                     alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler:nil))
                     self.present(alert, animated: true, completion:nil)
                }
                else{
                    let alert = UIAlertController(title: "", message: "Welcome Back, " + user.first_name + "!", preferredStyle: UIAlertController.Style.alert)
                     alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { action in
                        
                        self.performSegue(withIdentifier: "showDashboard", sender: self)

                                               }))
                     self.present(alert, animated: true, completion:nil)
                    
                }
                self.activityIndicator.stopAnimating()
            }
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDashboard" {
            let dashBoardVC : DashboardVC = segue.destination as! DashboardVC
            dashBoardVC.user = self.usr
        }
    }
    
}

@IBDesignable extension UIView {

    @IBInspectable var borderWidth: CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }

    @IBInspectable var cornerRadius: CGFloat {
        set {
            layer.cornerRadius = newValue
        }
        get {
            return layer.cornerRadius
        }
    }

    @IBInspectable var borderColor: UIColor? {
        set {
            guard let uiColor = newValue else { return }
            layer.borderColor = uiColor.cgColor
        }
        get {
            guard let color = layer.borderColor else { return nil }
            return UIColor(cgColor: color)
        }
    }
}

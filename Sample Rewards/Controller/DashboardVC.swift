//
//  DashboardVC.swift
//  Sample Rewards
//
//  Created by Chris Blay on 11/20/20.
//

import UIKit

class DashboardVC: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblMobileNo: UILabel!
    var user: User!
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        // make a cell for each cell index path
               
               // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath)
               
               // Use the outlet in our custom class to get a reference to the UILabel in the cell
               
               
               
               return cell
           
    }
    
    @IBAction func btnLogoutTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        

            
                
              let headerView = collectionView.dequeueReusableSupplementaryView(
                ofKind: kind,
                withReuseIdentifier: "cellheader", for: indexPath)
              
        let lbl1 = headerView.subviews[0] as?  UILabel
        let lbl2 = headerView.subviews[1] as?  UILabel

        lbl1?.text = user.first_name + " " + user.last_name
        lbl2?.text = user.mobile
        
        return headerView
    }
            
            
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = self.view.bounds.width - 20
        let height: CGFloat = 200
        return CGSize(width: width, height: height)
    }
    
  
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
}

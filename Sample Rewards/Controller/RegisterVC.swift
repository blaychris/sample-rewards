//
//  RegisterVC.swift
//  Sample Rewards
//
//  Created by Chris Blay on 11/19/20.
//

import UIKit
import Firebase

class RegisterVC: UIViewController, UITextFieldDelegate, UIAlertViewDelegate {
    
    
    @IBOutlet weak var txtFirstName: UITextField!
    
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtMobileNo: UITextField!
    @IBOutlet weak var txtMPIN: UITextField!
    @IBOutlet weak var txtConfirm: UITextField!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var errFirstName: UILabel!
    @IBOutlet weak var errLastName: UILabel!
    @IBOutlet weak var errMobileNo: UILabel!
    @IBOutlet weak var errMPIN: UILabel!
    @IBOutlet weak var errConfirm: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    
    @objc func adjustForKeyboard(notification: Notification) {
        guard let keyboardValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }

        let keyboardScreenEndFrame = keyboardValue.cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)


        
        if notification.name == UIResponder.keyboardWillHideNotification {
            scrollView.contentInset = .zero
        } else {
            let lastY = txtConfirm.superview!.frame.origin.y + 60
            
            if self.view.bounds.height - keyboardViewEndFrame.height < lastY {
                scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height - 140 - view.safeAreaInsets.bottom, right: 0)
            }
            else{
                scrollView.contentInset = .zero
            }
        }

        scrollView.scrollIndicatorInsets = scrollView.contentInset


    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillHideNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        
        let tap = UITapGestureRecognizer(target: view, action: #selector(UIView.endEditing))
        view.addGestureRecognizer(tap)
        
        let toolbar = UIToolbar()
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace,
                                        target: nil, action: nil)
        let btnKeyboardDone = UIBarButtonItem(title: "Done", style: .done,
                                         target: self, action: #selector(doneButtonTapped))
        
        toolbar.setItems([flexibleSpace, btnKeyboardDone], animated: true)
        toolbar.sizeToFit()
        
        txtFirstName.inputAccessoryView = toolbar
        txtLastName.inputAccessoryView = toolbar
        txtMobileNo.inputAccessoryView = toolbar
        txtMPIN.inputAccessoryView = toolbar
        txtConfirm.inputAccessoryView = toolbar
     
    }
    
    @objc func doneButtonTapped() {
        view.endEditing(true)
    }
    
    @IBAction func backButtonTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField.tag < 2 {
            return true
        }
        
        if range.location < 2 && textField.tag == 2{
            if string == ""{
                return true
            }
            if !(((range.location == 0) && (string == "0")) || ((range.location == 1) && (string == "9"))){
                return false
            }
        }
        
        
        let maxLength = (textField.tag == 2) ? 11 : 4
        
        if range.location > maxLength - 1 {
            if textField.tag == 2 {
                txtMPIN.becomeFirstResponder()
            }else if textField.tag == 3 {
                txtConfirm.becomeFirstResponder()
            }else{
                doneButtonTapped()
            }
            
            return false
        }

        return true
    }
    
//    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        if textField.tag == 0 {
//            txtLastName.becomeFirstResponder()
//        }
//        else{
//            txtMobileNo.becomeFirstResponder()
//        }
//        return true
//    }
    
    @IBAction func btnRegisterTapped() {
        validateInputs()
    }
    
    func validateInputs(){
        var noError = true
        errFirstName.text = ""
        errLastName.text = ""
        errMobileNo.text = ""
        errMPIN.text = ""
        errConfirm.text = ""
        
        txtFirstName.text = txtFirstName.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        txtLastName.text = txtLastName.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if txtFirstName.text!.count == 0  {
            errFirstName.text = "First name should'nt be empty"
            noError = false
        }
        if txtLastName.text!.count == 0  {
            errLastName.text = "Last name should'nt be empty"
            noError = false
        }
        if txtMobileNo.text!.count != 11  {
            errMobileNo.text = "Mobile number should be in 09XXXXXXXXX format"
            noError = false
        }
        if txtMPIN.text!.count != 4 {
            errMPIN.text = "MPIN should be 4-digits"
            noError = false
        }else{
            if txtMPIN.text! != txtConfirm.text! {
                errConfirm.text = "MPINs not the same"
                noError = false
            }
        }
        
        if noError {
            handleRegistration()
        }
        
    }
    
    func handleRegistration(){
        activityIndicator.startAnimating()
        let credentials = AuthCredentials(first_name: txtFirstName.text!, last_name: txtLastName.text!, mobile: txtMobileNo.text!, mpin: txtMPIN.text!)
       
        let ref = Database.database().reference()
        ref.child("users").child(credentials.mobile).observeSingleEvent(of: .value, with: {snapshot in
            if snapshot.exists(){
                self.activityIndicator.stopAnimating()
                self.errMobileNo.text = "This mobile number is already registered"
                
            }else{

                AuthService.shared.registerUser(credentials: credentials) { (error, ref) in
                    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let loginVC = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC

                    guard var viewControllers = self.navigationController?.viewControllers else { return }

                    // Popped ViewController not used
                    _ = viewControllers.popLast()

                    // Push targetViewController
                    viewControllers.append(loginVC)
                    
                    
                    let alert = UIAlertController(title: "", message: "Account Successfully Created", preferredStyle: UIAlertController.Style.alert)
                     alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { action in
                        self.navigationController?.setViewControllers(viewControllers, animated: true)

                                               }))
                     self.present(alert, animated: true, completion:nil)
                    self.activityIndicator.stopAnimating()
                    


                }
                
            }
        })
        
        
        
        
        
    }
    
    
    

    
}


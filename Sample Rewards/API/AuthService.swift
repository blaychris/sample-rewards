//
//  UserService.swift
//  Sample Rewards
//
//  Created by Chris Blay on 11/19/20.
//

import Foundation
import Firebase

struct  AuthCredentials {
    let first_name: String
    let last_name: String
    let mobile: String
    let mpin: String
}

struct AuthService {
    static let shared = AuthService()
    
    func registerUser(credentials: AuthCredentials, completion: @escaping(Error? , DatabaseReference) -> Void) {
        
        let ref = Database.database().reference()
                
            
                ref.child("users").child(credentials.mobile).setValue(
                    ["first_name": credentials.first_name,
                     "last_name":credentials.last_name,
                     "mobile":credentials.mobile,
                     "mpin":credentials.mpin
                    ], withCompletionBlock: completion
                )
        
        
    }
    

    
}

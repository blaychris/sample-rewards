//
//  UserService.swift
//  Sample Rewards
//
//  Created by Chris Blay on 11/20/20.
//

import Firebase

struct  UserService {
    static let shared = UserService()
    
    func fetchUserBy(mobileNo: String, completion: @escaping(User) -> Void){
        let ref = Database.database().reference()
        ref.child("users").child(mobileNo).observeSingleEvent(of: .value, with: {snapshot in
            guard let dictionary = snapshot.value as? [String:AnyObject] else {return}
            
            let user = User(user_id: mobileNo, dictionary: dictionary)
            completion(user)
        })
        
        
        
    }
}
